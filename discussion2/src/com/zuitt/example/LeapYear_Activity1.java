package com.zuitt.example;

import java.util.Scanner;

public class LeapYear_Activity1 {

    public static void main(String[] args){
        Scanner input = new Scanner(System.in);

        System.out.println("Enter year to check if it's a leap year: ");
        int year = input.nextInt();

        if(year % 4 == 0 && year % 100 != 0 || year % 400 == 0){
            System.out.println(year + " is a leap year");
        }
        else{
            System.out.println(year + " is not a leap year");
        }
    }
}
