package com.zuitt.example;

import java.util.Scanner;

public class SelectionControl {

    public static void main(String[] args){

        // [SECTION] Java Operators
        // Arithmetic +  -  *  /  %
        // Comparison <  >  >=  <=  ==  !=
        // Logical &&  ||  !
        // Assignment =

        // a = a + 1 is the same with a += 1;

        // [SECTION] Control Structures in Java
        // Syntax:
            /*if(condition){
                code block
            }
            else{
                code block
            }*/

        // Example:
        int num1 = 36;

        if(num1 % 5 == 0){
            System.out.println(num1 + " is divisible by 5");
        }
        else{
            System.out.println(num1 + " is not divisible by 5");
        }

        // [SECTION] Short Circuiting
        int x = 15;
        int y = 0;

        if(y != 0 && x/y == 0){
            System.out.println("Result is: " + x/y);
        }
        else{
            System.out.println("This will only run because of short circuiting");
        }

        // [SECTION] Ternary Operator
        int num2 = 24;
        Boolean result = (num2 > 0) ? true : false;
        System.out.println(result);

        // [SECTION] Switch Cases
        Scanner numberScanner = new Scanner(System.in);
        System.out.println("Enter a number");
        int directionValue = numberScanner.nextInt();

        switch (directionValue){
            case 1:
                System.out.println("North");
                break;
            case 2:
                System.out.println("South");
                break;
            case 3:
                System.out.println("West");
                break;
            case 4:
                System.out.println("East");
                break;
            default:
                System.out.println("Invalid input. Please select from 1-4");
                break;
        }

    }
}
