package com.zuitt.example;

import java.util.Scanner;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;


public class PrimeNumber_Activity2 {

    public static void main(String[] args) {

        // Array
        int [] primeArr = {2, 3, 5, 7, 11};
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter index of prime number: ");

        int num = scanner.nextInt();

        switch (num) {
            case 0:
                System.out.println("First prime number: " + primeArr[num]);
                break;
            case 1:
                System.out.println("Second prime number: " + primeArr[num]);
                break;
            case 2:
                System.out.println("Third prime number: " + primeArr[num]);
                break;
            case 3:
                System.out.println("Fourth prime number: " + primeArr[num]);
                break;
            case 4:
                System.out.println("Fifth prime number: " + primeArr[num]);
                break;
            default:
                System.out.println("Invalid input. Please select from 1-5");
        }


        // ArrayList
        ArrayList<String> myArrayList = new ArrayList<String>(Arrays.asList("Adrian, Dimple, Justine, Niza"));
        System.out.println("My friends are: " + myArrayList);


        // HashMap
        HashMap<String, Integer> myHashMap = new HashMap<String, Integer>() {
            {
                put("toothpaste", 15);
                put("toothbrush", 20);
                put("soap", 12);
            }
        };

        System.out.println("Our current inventory consists of: " + myHashMap);



    }

}
